import tensorflow as tf
from .preprocessing.inception_preprocessing import preprocess_image
import pandas as pd
import numpy as np
from collections import OrderedDict
from imblearn.over_sampling import RandomOverSampler
from sklearn.utils import resample, shuffle



def _parse_function_train(filename, label):
    img_string = tf.io.read_file(filename)
    image = tf.image.decode_jpeg(img_string, channels=3)
    image = tf.cast(image, tf.float32) / 255.0
    image = preprocess_image(image) ######TO DO
    return image, label


def _parse_function_test(filename, label):
    img_string = tf.io.read_file(filename)
    image = tf.image.decode_jpeg(img_string, channels=3)
    image = tf.cast(image, tf.float32) / 255.0
    image = preprocess_image(image)  ######TO DO
    return image, label

def _data_upsample(img_paths, labels, weight_file):
    label_data = {}
    label_num_samples = {}

    for img_path, label in zip(img_paths, labels):
        if label not in label_data:
            label_data[label] = []
        label_data[label].append(img_path)

    img_paths = []
    labels = []

    class_weights = get_class_weights(weight_file)
    class_weights -= 2.0
    class_weights = np.where(class_weights > 0, class_weights, np.zeros_like(class_weights))

    for label, label_img_paths in label_data.items():
        img_paths += label_img_paths
        labels += [label] * len(label_img_paths)
        num_samples = int(class_weights[label] * len(label_img_paths))
        img_paths += resample(label_img_paths, n_samples=num_samples, replace=True)

        labels += [label] * num_samples
        label_num_samples[label] = num_samples

    img_paths, labels = shuffle(img_paths, labels)
    return img_paths, labels


def _data_upsample1(img_paths, labels):
    ros = RandomOverSampler()
    return img_paths, labels

def _read_data_file(data_file, up_sample=False, weight_file=None):
    print("Loading data: {}".format(data_file))
    df = pd.read_csv(data_file, header=None)
    img_paths = df[0].values
    labels = df[1].values

    # Data up sampling
    if up_sample:
        img_paths, labels = _data_upsample(img_paths, labels, weight_file)

    return img_paths, labels

def get_class_weights(weight_file):
    with open(weight_file, 'r') as f:
        id_weight_dic = {}
        for line in f:
          id, weight = line.strip().split('\t')
          id_weight_dic[int(id)] = float(weight)

        class_weights = []
        for _, weight in OrderedDict(sorted(id_weight_dic.items())).items():
          class_weights.append(weight)

        class_weights = np.array(class_weights)
        class_weights = np.max(class_weights) / class_weights
        class_weights = np.log(class_weights)

        return class_weights

    