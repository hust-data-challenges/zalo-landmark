from libs.modeling.recognition.model_zoo import *
import tensorflow as tf

slim = tf.contrib.slim

networks_map = {'inception_v4': inception.inception_v4,
                'inception_resnet_v2': inception.inception_resnet_v2,
                'resnet_v2_50': resnet_v2.resnet_v2_50,
                'resnet_v2_101': resnet_v2.resnet_v2_101,
                'resnet_v2_152': resnet_v2.resnet_v2_152,
                'resnet_v2_200': resnet_v2.resnet_v2_200,
                'nasnet_large': nasnet.build_nasnet_large,
                'pnasnet_large': pnasnet.build_pnasnet_large,
                'densenet161': densenet.densenet161,
                }

arg_scopes_map = {'inception_v4': inception.inception_v4_arg_scope,
                  'inception_resnet_v2': inception.inception_resnet_v2_arg_scope,
                  'resnet_v2_50': resnet_v2.resnet_arg_scope,
                  'resnet_v2_101': resnet_v2.resnet_arg_scope,
                  'resnet_v2_152': resnet_v2.resnet_arg_scope,
                  'resnet_v2_200': resnet_v2.resnet_arg_scope,
                  'nasnet_large': nasnet.nasnet_large_arg_scope,
                  'pnasnet_large': pnasnet.pnasnet_large_arg_scope,
                  'densenet161': densenet.densenet_arg_scope,
                  }


class Recognizer(object):

    def __init__(self, cfg):
        self.func = networks_map[cfg['NET']]
        self.arg_scope = arg_scopes_map[cfg['NET']]
        self.num_classes = cfg['NUM_CLASSES']

    def __call__(self, inputs, training, **kwargs):
        with slim.arg_scope(self.arg_scope):
            return self.func(inputs, self.num_classes, is_training=training, **kwargs)

