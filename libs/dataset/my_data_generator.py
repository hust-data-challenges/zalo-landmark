import tensorflow as tf
import numpy as np
import imgaug
from libs.utils.dataset import _read_data_file, _parse_function_test, _parse_function_train


class ZL_Generator(object):

    def __init__(self, cfg, train_map_fn=None, test_map_fn=None, augmenter=None):
        self.batch_size = cfg['BATCH_SIZE']
        self.buffer_size = cfg['BUFFER_SIZE']
        self.num_threads = cfg['NUM_THREADS']
        self.shuffle = cfg['TRAIN']['SHUFFLE'] == 'True'
        self.train_map_fn = train_map_fn
        self.test_map_fn = test_map_fn

        self.augmenter = augmenter

        self.train_img_paths, self.train_labels = _read_data_file(cfg['TRAIN']['DATA_FILE'],
                                                                  cfg['TRAIN']['UPSAMPLE'] == 'True',
                                                                  cfg['WEIGHT_FILE'])
        self.test_img_paths, self.test_labels = _read_data_file(cfg['TEST']['DATA_FILE'])

        self._build_train_set()
        self._build_test_set()


    def _generator(self, map_fn):
        img, target = None, None
        yield img, target

    def load_train_set(self, sess):
        pass

    def load_test_set(self, sess):
        pass

    def _build_data_set(self, map_fn, shuffle=False):
        data = tf.data.Dataset.from_generator(
            self._generator,
            output_types=self.train_set.output_types,
            output_shapes=self.train_set.output_shapes,
            args=map_fn
        )
        if shuffle:
            data = data.shuffle(buffer_size=self.buffer_size)
        data.batch(self.batch_size)
        data = data.prefetch(self.batch_size)
        return data

    def _build_train_set(self):
        if self.train_map_fn == None:
            self.train_map_fn = _parse_function_train
        self.train_set = self._build_data_set(self.train_map_fn, self.shuffle)

    def _build_test_set(self):
        if self.test_map_fn == None:
            self.test_map_fn = _parse_function_test
        self.test_set = self._build_data_set(self.test_map_fn, self.shuffle)

    def get_next(self, sess):
        pass
