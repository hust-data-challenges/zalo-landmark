import tensorflow as tf
from libs.utils.dataset import _read_data_file, _parse_function_train, _parse_function_test
import numpy as np


class ZL_Generator(object):
    def __init__(self, cfg, train_map_fn=None, test_map_fn=None, augmenter=None):
        self.batch_size = cfg['BATCH_SIZE']
        self.buffer_size = cfg['BUFFER_SIZE']
        self.num_threads = cfg['NUM_THREADS']
        self.shuffle = cfg['TRAIN']['SHUFFLE'] == 'True'
        self.train_map_fn = train_map_fn
        self.test_map_fn = test_map_fn

        self.augmenter = augmenter

        self.train_img_paths, self.train_labels = _read_data_file(cfg['TRAIN']['DATA_FILE'],
                                                                  cfg['TRAIN']['UPSAMPLE'] == 'True',
                                                                  cfg['WEIGHT_FILE'])
        self.test_img_paths, self.test_labels = _read_data_file(cfg['TEST']['DATA_FILE'])

        self.train_batches_per_epoch = int(np.ceil(len(self.train_labels) / self.batch_size))
        self.test_batches_per_epoch = int(np.ceil(len(self.test_labels) / self.batch_size))
        # print(self.train_batches_per_epoch)

        # build datasets
        self._build_train_set()
        self._build_test_set()

        self.iterator = tf.data.Iterator.from_structure(
            self.train_set.output_types,
            self.train_set.output_shapes
        )

        self.train_init_opt = self.iterator.make_initializer(self.train_set)
        self.test_init_opt = self.iterator.make_initializer(self.test_set)
        self.next = self.iterator.get_next()

    def load_train_set(self, sess):
        return sess.run(self.train_init_opt)

    def load_test_set(self, sess):
        return sess.run(self.test_init_opt)

    def get_next(self, sess):
        return sess.run(self.next)

    def _build_data_set(self, img_paths, labels, map_fn, shuffle=False):
        img_paths = tf.convert_to_tensor(img_paths, dtype=tf.string)
        labels = tf.convert_to_tensor(labels, dtype=tf.int32)
        data = tf.data.Dataset.from_tensor_slices((img_paths, labels))
        if shuffle:
            data = data.shuffle(buffer_size=self.buffer_size)
        data = data.map(map_fn, num_parallel_calls=self.num_threads)
        data = data.batch(self.batch_size)
        data = data.prefetch(self.batch_size)
        return data

    def _build_train_set(self):
        if self.train_map_fn == None:
            self.train_map_fn = _parse_function_train
        self.train_set = self._build_data_set(self.train_img_paths,
                                              self.train_labels,
                                              self.train_map_fn,
                                              self.shuffle)

    def _build_test_set(self):
        if self.test_map_fn == None:
            self.test_map_fn = _parse_function_test
        self.test_set = self._build_data_set(self.test_img_paths,
                                             self.test_labels,
                                             self.test_map_fn)