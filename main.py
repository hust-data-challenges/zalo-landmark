from libs.dataset.data_generator import ZL_Generator
from libs.utils.misc import config_parser


if __name__ == "__main__":
    cfg = config_parser('configs/inception_resnet_v2.yml')
    dataset = ZL_Generator(cfg['DATASET'])
